from yarrow2 import connection

def main():
    c = connection(hostname='127.0.0.1')
    print(c.next())
    print(c.send('HELP'))
    print(c.send('MOTD'))
    print(c.send('QUIT'))

if __name__=='__main__':
    main()
