import unittest
import subprocess
import socket
import sys
import os
from glob import glob
import tempfile
import datetime

import yarrow2.rgtplib

TEST_ITEM_CONTENTS = [
        {'sequence': 0,
            'timestamp': datetime.datetime(2019, 3, 7, 22, 54, 19),
            'author': 'guest-write',
            'grogname': 'I like wombats.',
            'contents': 'This is a test item. Hello.\n',
            },
        {'sequence': 2,
            'timestamp': datetime.datetime(2019, 3, 7, 23, 2, 51),
            'author': 'guest-write',
            'grogname': 'I like trees',
            'contents': 'And cats\nAnd rabbits are also nice\n',
            },
        {'sequence': 6,
            'timestamp': datetime.datetime(2019, 3, 7, 23, 5, 11),
            'author': 'guest-write',
            'grogname': 'I like beer',
            'contents': "I don't even know what Spong means :(\n",
            },
        {'sequence': 7,
            'timestamp': datetime.datetime(2019, 3, 7, 23, 5, 34),
            'author': 'guest-write',
            'grogname': 'I like cats',
            'contents': 'I think he was bishop of New Jersey a while back?\n\n',
            },
        ]

stock_text = dict(
        [(os.path.basename(fn)[6:], open(fn, 'r').read())
        for fn in glob(
            os.path.join(os.path.dirname(__file__),
                'stock.*',
                ))
        ])

class YarrowTest(unittest.TestCase):

    def setUp(self):
        self._socket = socket.socket()

        self._socket.bind(('',0))
        self._port = self._socket.getsockname()[1]
        self._socket.listen(0)
        self.vault = tempfile.mkdtemp(prefix='yarrow')

        for fn, contents in self._vault_contents():

            if contents is None:
                contents = stock_text[fn]

            f = open(os.path.join(self.vault, fn), 'w')
            f.write(contents)
            f.close()

        if os.fork()==0:
            # we are the child
            conn, addr = self._socket.accept()

            os.close(0)
            os.dup2(conn.fileno(), 0)
            os.close(1)
            os.dup2(conn.fileno(), 1)

            os.execl(
                    sys.executable,
                    sys.executable,
                    os.path.join(
                        os.path.dirname(__file__),
                        'spurge_rgtpd.py',
                        ),
                    '--vault',
                    self.vault,
                    )

            raise RuntimeError('impossible')

        else:
            self._set_up_client()

    def _set_up_client(self):
        self.yarrow = yarrow2.rgtplib.Yarrow(hostname='0.0.0.0', port=self._port)

    def _vault_contents(self):
        return [
                ('vault.conf', None),
                ]

    def tearDown(self):
        self._socket.close()

        if self.vault is not None:
            try:
                for f in glob(os.path.join(self.vault, '*')):
                        os.unlink(f)

                os.rmdir(self.vault)
            except Exception as e:
                print('Problem removing stuff: '+str(e))
                pass

    def assertResponse(self,
            response,
            code = None,
            message = None,
            data = None,
            ):

        if code is not None:
            self.assertEqual(response.code, code,
                    msg=str(response))

        if message is not None:
            self.assertIn(message, response.message,
                    msg=str(response))

        if data is not None:
            self.assertIn(data, response.data)

class ConnectionTest(YarrowTest):

    def _set_up_client(self):
        self.conn = yarrow2.rgtplib.connection(hostname='0.0.0.0', port=self._port)

class test_basic(ConnectionTest):

    def test_connection(self):
        self.assertResponse(
                self.conn.send(None),
                230)
        self.assertResponse(
                self.conn.send('HELP'),
                250)
        self.assertResponse(
                self.conn.send('QUIT'),
                280)

    def test_no_motd(self):
        self.assertResponse(
                self.conn.send(None),
                230)
        with self.assertRaises(yarrow2.rgtplib.RGTPException):
            self.assertResponse(
                    self.conn.send('MOTD'),
                    410)

class test_motd(ConnectionTest):

    def _vault_contents(self):
        return [
                ('vault.conf', None),
                ('motd', """00000000 00000000
Welcome to this unconfigured RGTP server."""),
                ]

    def test_MOTD(self):
        self.assertResponse(
                self.conn.send(None),
                230)
        self.assertResponse(
                self.conn.send('HELP'),
                250)
        self.assertResponse(
                self.conn.send('MOTD'),
                code = 250,
                data = 'unconfigured')
        self.assertResponse(
                self.conn.send('QUIT'),
                280)

class GuestLoginTest(ConnectionTest):

    def _vault_contents(self):
        return [
                ('vault.conf', None),
                ('users.conf', None),
                ]
        
    def _set_up_client(self):
        self.yarrow = yarrow2.rgtplib.Yarrow(
                hostname='0.0.0.0',
                port=self._port,
                )

    def login(self, username, expectedLevel):
        self.yarrow.login(username)
        self.assertEqual(self.yarrow.access_level,
                expectedLevel)

class test_guest_read(GuestLoginTest):
    def test_login(self):
        self.login('guest-read', 1)

class test_guest_write(GuestLoginTest):
    def test_login(self):
        self.login('guest-write', 2)

class test_guest_edit(GuestLoginTest):
    def test_login(self):
        self.login('guest-edit', 3)

class test_login_unknown(GuestLoginTest):
    def test_login(self):
        with self.assertRaises(LookupError):
            self.yarrow.login('wombat')

class IndexTest(YarrowTest):
    def _vault_contents(self):
        return [
                ('vault.conf', None),
                ('users.conf', None),
                ('index', None),
                ('I1506061', None),
                ('I0526596', None),
                ]

    def test_index(self):
        self.yarrow.login('guest-read')

        idx = self.yarrow.index()

        for line, (serial, timestamp, itemid, user, type_, subject) in zip(idx, [
                (0, '2019-03-07T22:54:19', 'I1506061', 'guest-write', 'I', 'Spong'),
                (2, '2019-03-07T23:02:51', 'I1506061', 'guest-write', 'R', 'Spong'),
                (3, '2019-03-07T23:03:52', 'I0526596', 'guest-write', 'I', 'Anyone for a game?'),
                (4, '2019-03-07T23:04:26', 'I0526596', 'guest-write', 'R', 'Anyone for a game?'),
                (5, '2019-03-07T23:04:51', 'I0526596', 'guest-write', 'R', 'Anyone for a game?'),
                (6, '2019-03-07T23:05:11', 'I1506061', 'guest-write', 'R', 'Spong'),
                (7, '2019-03-07T23:05:34', 'I1506061', 'guest-write', 'R', 'Spong'),
                ]):

            timestamp = datetime.datetime.strptime(timestamp,
                    "%Y-%m-%dT%H:%M:%S",
                    )

            self.assertEqual(serial,    line.serial,    msg=str(line))
            self.assertEqual(timestamp, line.timestamp, msg=str(line))
            self.assertEqual(itemid,    line.itemid,    msg=str(line))
            self.assertEqual(user,      line.user,      msg=str(line))
            self.assertEqual(type_,     line.type,      msg=str(line))
            self.assertEqual(subject,   line.subject,   msg=str(line))

class ItemTest(YarrowTest):
    def _vault_contents(self):
        return [
                ('vault.conf', None),
                ('users.conf', None),
                ('index', None),
                ('I1506061', None),
                ('I0526596', None),
                ]

    def test_item(self):
        self.yarrow.login('guest-read')

        item = self.yarrow.item('I1506061')

        self.assertEqual(item.prev, None)
        self.assertEqual(item.next, None)
        self.assertEqual(item.last_edit, None)
        self.assertEqual(item.last_reply, 7)

        for received, wanted in zip(item.entries, TEST_ITEM_CONTENTS):
            self.assertEqual(received.sequence, wanted['sequence']),
            self.assertEqual(received.timestamp, wanted['timestamp']),
            self.assertEqual(received.author, wanted['author']),
            self.assertEqual(received.grogname, wanted['grogname']),
            self.assertEqual(received.contents_as_string(), wanted['contents']),

class PostTest(YarrowTest):
    def _vault_contents(self):
        return [
                ('vault.conf', None),
                ('users.conf', None),
                ('index', None),
                ]

    def test_post(self):
        self.yarrow.login('guest-write')

        itemid1 = self.yarrow.post(
                text = "There are two major kinds of camel.",
                subject = "I like camels.",
                grogname = 'grogname thing',
                )
        itemid2 = self.yarrow.post(
                text = "There are so many kinds of beer!",
                subject = "I like beer.",
                grogname = 'grogname thing',
                )

        item1 = self.yarrow.item(itemid1['itemid'])
        self.assertIn('camel', item1.entries[0].subject)
        item2 = self.yarrow.item(itemid2['itemid'])
        self.assertIn('beer',  item2.entries[0].subject)

class ReplyTest(YarrowTest):
    def _vault_contents(self):
        return [
                ('vault.conf', None),
                ('users.conf', None),
                ('index', None),
                ]

    def test_post(self):
        self.yarrow.login('guest-write')

        first = self.yarrow.post(
                text = "There are two major kinds of camel.",
                subject = "I like camels.",
                grogname = 'grogname thing',
                )
        second = self.yarrow.post(
                text = "I know about dromedaries. What's the other one?",
                reply_to = first['itemid'],
                grogname = 'camel confusion',
                )

        item = self.yarrow.item(first['itemid'])
        self.assertIn('major', item.entries[0].contents_as_string())
        self.assertIn('dromedaries', item.entries[1].contents_as_string())
