import socket
from hashlib import md5
import random
import datetime

def random_hex_string(length = 32):

        result = ''
        # The easiest way to generate a hex digit is using the built-in
        # hex() function, which returns strings of the form "0x7"-- so
        # we take the third character.
        for n in range(0, length):
                result = result + hex(int(random.random()*16))[2]

        return result

class RGTPException(Exception):
    def __init__(self, response):
        super().__init__(str(response))
        self.response = response

def connection(
            hostname='rgtp-serv.groggs.group.cam.ac.uk',
            port=1431,
            ):

    class ConnectionResult(object):
        def __init__(self, line, previous=None):
            self.code = int(line[0:3])
            self.message = line[4:].rstrip()
            self.data = None
            self.previous = previous

        def __repr__(self):
            result = '%03d %s' % (self.code, self.message)

            if self.data is not None:
                result += '\n' + self.data

            if self.previous is not None:
                result += ' (previously: ' + str(self.previous) + ")"

            return result
 
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    sock.connect((hostname, port))

    incoming = sock.makefile('r')
    outgoing = sock.makefile('w')
    
    partial_result = None

    try:
        while True:
            rec = incoming.readline()
            if rec is None:
                return

            result = ConnectionResult(rec,
                    previous = partial_result)

            partial_result = None

            if result.code==250:
                data = ''
                reading = True
                while reading:
                    temp = incoming.readline()

                    if temp.rstrip()=='.':
                        reading = False
                    else:
                        data += temp
                result.data = data

            if result.code>=400 and result.code<=499:
                raise RGTPException(result)
            elif result.code>=100 and result.code<=199:
                partial_result = result
            else:
                send = yield result

                if send is not None:
                    outgoing.write(send+'\r\n')
                    outgoing.flush()

    finally:
        sock.close()

def _make_data(grogname, text):
    return "DATA\r\n{}\r\n{}\r\n.".format(grogname, text)

def _maybe_none(a, as_type=None):
    if a.strip() is '':
        return None

    if as_type is None:
        return a
    elif as_type==datetime.datetime:
        return datetime.datetime.utcfromtimestamp(int(a, 16))
    elif as_type==int:
        return int(a, 16)
    else:
        raise ValueError("Can't convert to type "+str(as_type))

class IndexEntry(object):

    def __init__(self, source):
        self.serial = int(source[0:8], 16)
        self.timestamp = _maybe_none(source[9:17],
                as_type=datetime.datetime)
        self.itemid = source[18:26]
        self.user = source[27:102].strip()
        self.type = source[103]
        self.subject = source[105:].strip()

    def __repr__(self):
        return str(self.as_tuple())

    def as_tuple(self):
        return (
            self.serial,
            self.timestamp.isoformat(),
            self.itemid,
            self.user,
            self.type,
            self.subject,
            )

class ItemEntry(object):
    def __init__(self, sequence, timestamp):
        self.sequence = sequence
        self.timestamp = timestamp
        self.author = None
        self.grogname = None
        self.subject = None
        self.is_reply = None
        self.itemid = None
        self.contents = []

    def add_line(self, line):
        if self.is_reply is None:
            # First line.
            # This must be "Item [itemid] from..." or "Reply from...".

            if line.startswith('Item '):
                self.is_reply = False
                self.itemid = line[5:13]
                line = line[19:]
            elif line.startswith('Reply from '):
                self.is_reply = True
                line = line[11:]

            atpos = line.find(' at ')

            if atpos!=-1:
                # so the line goes "...from [author] at [timestamp]"
                self.author = line[:atpos]

            # Otherwise they'll tell us the author later.

        elif self.grogname is None and line.startswith('From '):
            self.grogname = line[5:]

        elif self.subject is None and line.startswith('Subject: '):
            # we don't actually use this, but let's parse it out
            self.subject = line[9:]

        elif line=='' and len(self.contents)==0:
            pass # ignore blank leading lines
        else:
            self.contents.append(line)

            # If we've got this far but we don't know the grogname,
            # it must be in the author field

            if self.grogname is None and self.author is not None:
                openbracket = self.author.find('(')

                if openbracket==-1:
                    # well, just give them the author name again
                    self.grogname = self.author
                else:
                    self.grogname = self.author[:openbracket-1]
                    self.author = self.author[openbracket+1:-1]
                    
    def contents_as_string(self):
        return '\n'.join(self.contents)

    def as_dict(self):
        result = {
                'sequence': self.sequence,
                'timestamp': self.timestamp,
                'reply?': self.is_reply,
                'itemid': self.itemid,
                'author': self.author,
                'grogname': self.grogname,
                'contents': self.contents,
                }

        return result

    def __repr__(self):
        result = ''
        a = self.as_dict()

        for name, value in a.items():
            if name=='contents':
                continue
            result += '\t{}: {}\n'.format(name.title(), str(value))

        result += '\t>\t'+ ('\n\t>\t'.join(self.contents)) + '\n'

        result += '\t----\n\n'

        return result

class Item(object):
    def __init__(self, source):
        lines = source.split('\n')

        metadata = lines.pop(0)
        self.prev = _maybe_none(metadata[0:8])
        self.next = _maybe_none(metadata[9:17])
        self.last_edit = _maybe_none(metadata[18:26], as_type=int)
        self.last_reply = _maybe_none(metadata[27:35], as_type=int)
        self.subject = metadata[36:]

        self.entries = []
        for line in lines:

            if line.startswith('^') and not line.startswith('^^'):
                self.entries.append(ItemEntry(
                    sequence=_maybe_none(line[1:9], as_type=int),
                    timestamp=_maybe_none(line[10:18], as_type=datetime.datetime),
                    ))
            else:
                if line.startswith('^^'):
                    line = line[1:]

                if len(self.entries)==0:
                    raise ValueError('extraneous text before first entry')

                self.entries[-1].add_line(line)

    def as_dict(self):
        result = {
                'entries': self.entries,
                'prev': self.prev,
                'next': self.next,
                'last edit': self.last_edit,
                'last reply': self.last_reply,
                'entries': self.entries,
                }

        return result

    def __repr__(self):
        a = self.as_dict()
        result = ''

        for name, value in a.items():
            if name=='entries':
                continue
            result += '{}: {}\n'.format(name.title(), str(value))

        for entry in self.entries:
            result += str(entry)

        return result


class Yarrow(object):

    def __init__(self,
            hostname='rgtp-serv.groggs.group.cam.ac.uk',
            port=1431,
            ):
        self.conn = connection(
                hostname=hostname,
                port=port,
                )

        res = self.conn.send(None)
        if res.code!=230:
            raise ValueError("Can't log in: "+str(res))

    def login(self, username):
        try:
            res = self.conn.send("USER "+username)
        except RGTPException as r:
            del self.conn
            self.conn = None
            raise LookupError("User not found: "+username)

        if res.code in (231, 232, 233):
            self.access_level = res.code-230
            return self.access_level
        else:
            raise ValueError(str(res))

    def index(self):
        res = self.conn.send("INDX")
        if res.code!=250:
            raise ValueError(str(res))

        result = []

        for line in res.data.split('\n'):
            if line:
                result.append(IndexEntry(line))

        return result

    def item(self, itemid):
        res = self.conn.send("ITEM "+itemid)
        if res.code!=250:
            raise ValueError(str(res))

        return Item(res.data)

    def post(self, text,
            grogname,
            subject=None,
            reply_to=None):

        result = {}

        self.conn.send(_make_data(grogname, text))

        if reply_to is None:

            if subject is None:
                raise ValueError("You must specify a subject for a new item")
            r = self.conn.send("NEWI "+subject)
            result['itemid'] = r.previous.message

        else:

            result['itemid'] = reply_to
            r = self.conn.send("REPL "+reply_to)

        return result

